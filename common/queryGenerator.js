export const generateQueryString = (params) => {
  const queries = []

  if (params.page_size) {
    queries.push(`page_size=${params.page_size}`)
  }
  if (params.orderBy) {
    queries.push(`orderBy=${params.orderBy}`)
  }
  if (params.p) {
    queries.push(`p=${params.p}`)
  }

  queries.push('cache=0')
  const queryString = queries.join('&')
  return queryString
}
