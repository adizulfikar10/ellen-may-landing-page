export const state = () => ({
  pagination: { p: 1, page_size: 10, total_page: 10 },
  articles: []
});

export const getters = {
  getarticles(state) {
    return state.articles;
  }
};

export const actions = {
  async fetcharticles(context, payload) {
    try {
      // this.$axios.setToken('123', 'Bearer')
      const res = await this.$axios.$get("article/education");
      context.commit("setarticles", res);
      console.info(res);
    } catch (error) {
      console.info(error);
    }
  }
};

export const mutations = {
  setarticles(state, articles) {
    state.articles = articles.data;
  }
};
