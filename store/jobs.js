export const state = () => ({
  jobs: [],
  jobdetails: {}
});

export const getters = {
  getjobs(state) {
    return state.jobs;
  },

  getjobdetails(state) {
    return state.jobdetails;
  }
};

export const actions = {
  async fetchjobs(context, payload) {
    try {
      const res = await this.$axios.$get("obvest/jobs");
      console.info(res);
      context.commit("setjobs", res);
      console.info(res);
    } catch (error) {
      console.info(error);
    }
  },

  async fetchjobdetails(context, payload) {
    try {
      const res = await this.$axios.$get("obvest/jobs/" + payload.params.id);
      context.commit("setjobdetails", res);
    } catch (error) {
      console.info(error);
    }
  },

  async postjob(context, payload) {
    try {
      this.$axios.setHeader("Content-Type", "multipart/form-data");
      let formData = new FormData();
      for (const key in payload.body) {
        formData.append(key, payload.body[key]);
      }

      const res = await this.$axios.$post("obvest/applicant", formData);

      if (!res.error) {
        this.$toast.success(
          "Terimakasih atas ketertarikanya bergabung dengan Emtrade",
          {
            theme: "outline",
            position: "bottom-center",
            duration: 2000
          }
        );
      } else {
        this.$toast.error("Oopps Something wrong", {
          theme: "outline",
          position: "bottom-center",
          duration: 2000
        });
      }
      return res;
    } catch (error) {
      this.$toast.error("Oopps Something wrong", {
        theme: "outline",
        position: "bottom-center",
        duration: 2000
      });

      return error;
    }
  }
};

export const mutations = {
  setjobs(state, payload) {
    state.jobs = payload.data;
  },
  setjobdetails(state, payload) {
    state.jobdetails = payload;
  }
};
