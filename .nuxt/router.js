import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _29b7fa82 = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _70dd0380 = () => interopDefault(import('../pages/blog/index.vue' /* webpackChunkName: "pages/blog/index" */))
const _00ecf39c = () => interopDefault(import('../pages/career/index.vue' /* webpackChunkName: "pages/career/index" */))
const _1317785e = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _4329f454 = () => interopDefault(import('../pages/store.vue' /* webpackChunkName: "pages/store" */))
const _664a6e58 = () => interopDefault(import('../pages/testimonial.vue' /* webpackChunkName: "pages/testimonial" */))
const _c2543430 = () => interopDefault(import('../pages/blog/_id.vue' /* webpackChunkName: "pages/blog/_id" */))
const _2b1b2d04 = () => interopDefault(import('../pages/career/_id.vue' /* webpackChunkName: "pages/career/_id" */))
const _496fd547 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _29b7fa82,
    name: "about"
  }, {
    path: "/blog",
    component: _70dd0380,
    name: "blog"
  }, {
    path: "/career",
    component: _00ecf39c,
    name: "career"
  }, {
    path: "/login",
    component: _1317785e,
    name: "login"
  }, {
    path: "/store",
    component: _4329f454,
    name: "store"
  }, {
    path: "/testimonial",
    component: _664a6e58,
    name: "testimonial"
  }, {
    path: "/blog/:id",
    component: _c2543430,
    name: "blog-id"
  }, {
    path: "/career/:id",
    component: _2b1b2d04,
    name: "career-id"
  }, {
    path: "/",
    component: _496fd547,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config.app && config.app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
