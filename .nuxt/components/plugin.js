import Vue from 'vue'

const components = {
  DragDropFile: () => import('../../components/DragDropFile.vue' /* webpackChunkName: "components/drag-drop-file" */).then(c => c.default || c),
  LoadingBar: () => import('../../components/LoadingBar.vue' /* webpackChunkName: "components/loading-bar" */).then(c => c.default || c),
  Logo: () => import('../../components/Logo.vue' /* webpackChunkName: "components/logo" */).then(c => c.default || c),
  VuetifyLogo: () => import('../../components/VuetifyLogo.vue' /* webpackChunkName: "components/vuetify-logo" */).then(c => c.default || c)
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
