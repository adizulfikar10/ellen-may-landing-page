export { default as DragDropFile } from '../../components/DragDropFile.vue'
export { default as LoadingBar } from '../../components/LoadingBar.vue'
export { default as Logo } from '../../components/Logo.vue'
export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'

export const LazyDragDropFile = import('../../components/DragDropFile.vue' /* webpackChunkName: "components/drag-drop-file" */).then(c => c.default || c)
export const LazyLoadingBar = import('../../components/LoadingBar.vue' /* webpackChunkName: "components/loading-bar" */).then(c => c.default || c)
export const LazyLogo = import('../../components/Logo.vue' /* webpackChunkName: "components/logo" */).then(c => c.default || c)
export const LazyVuetifyLogo = import('../../components/VuetifyLogo.vue' /* webpackChunkName: "components/vuetify-logo" */).then(c => c.default || c)
