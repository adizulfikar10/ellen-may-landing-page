exports.ids = [12];
exports.modules = {

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(158);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("744bab4a", content, true)

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container.grow-shrink-0{flex-grow:0;flex-shrink:0}.container.fill-height{align-items:center;display:flex;flex-wrap:wrap}.container.fill-height>.row{flex:1 1 100%;max-width:calc(100% + 24px)}.container.fill-height>.layout{height:100%;flex:1 1 auto}.container.fill-height>.layout.grow-shrink-0{flex-grow:0;flex-shrink:0}.container.grid-list-xs .layout .flex{padding:1px}.container.grid-list-xs .layout:only-child{margin:-1px}.container.grid-list-xs .layout:not(:only-child){margin:auto -1px}.container.grid-list-xs :not(:only-child) .layout:first-child{margin-top:-1px}.container.grid-list-xs :not(:only-child) .layout:last-child{margin-bottom:-1px}.container.grid-list-sm .layout .flex{padding:2px}.container.grid-list-sm .layout:only-child{margin:-2px}.container.grid-list-sm .layout:not(:only-child){margin:auto -2px}.container.grid-list-sm :not(:only-child) .layout:first-child{margin-top:-2px}.container.grid-list-sm :not(:only-child) .layout:last-child{margin-bottom:-2px}.container.grid-list-md .layout .flex{padding:4px}.container.grid-list-md .layout:only-child{margin:-4px}.container.grid-list-md .layout:not(:only-child){margin:auto -4px}.container.grid-list-md :not(:only-child) .layout:first-child{margin-top:-4px}.container.grid-list-md :not(:only-child) .layout:last-child{margin-bottom:-4px}.container.grid-list-lg .layout .flex{padding:8px}.container.grid-list-lg .layout:only-child{margin:-8px}.container.grid-list-lg .layout:not(:only-child){margin:auto -8px}.container.grid-list-lg :not(:only-child) .layout:first-child{margin-top:-8px}.container.grid-list-lg :not(:only-child) .layout:last-child{margin-bottom:-8px}.container.grid-list-xl .layout .flex{padding:12px}.container.grid-list-xl .layout:only-child{margin:-12px}.container.grid-list-xl .layout:not(:only-child){margin:auto -12px}.container.grid-list-xl :not(:only-child) .layout:first-child{margin-top:-12px}.container.grid-list-xl :not(:only-child) .layout:last-child{margin-bottom:-12px}.layout{display:flex;flex:1 1 auto;flex-wrap:nowrap;min-width:0}.layout.reverse{flex-direction:row-reverse}.layout.column{flex-direction:column}.layout.column.reverse{flex-direction:column-reverse}.layout.column>.flex{max-width:100%}.layout.wrap{flex-wrap:wrap}.layout.grow-shrink-0{flex-grow:0;flex-shrink:0}@media (min-width:0){.flex.xs12{flex-basis:100%;flex-grow:0;max-width:100%}.flex.order-xs12{order:12}.flex.xs11{flex-basis:91.6666666667%;flex-grow:0;max-width:91.6666666667%}.flex.order-xs11{order:11}.flex.xs10{flex-basis:83.3333333333%;flex-grow:0;max-width:83.3333333333%}.flex.order-xs10{order:10}.flex.xs9{flex-basis:75%;flex-grow:0;max-width:75%}.flex.order-xs9{order:9}.flex.xs8{flex-basis:66.6666666667%;flex-grow:0;max-width:66.6666666667%}.flex.order-xs8{order:8}.flex.xs7{flex-basis:58.3333333333%;flex-grow:0;max-width:58.3333333333%}.flex.order-xs7{order:7}.flex.xs6{flex-basis:50%;flex-grow:0;max-width:50%}.flex.order-xs6{order:6}.flex.xs5{flex-basis:41.6666666667%;flex-grow:0;max-width:41.6666666667%}.flex.order-xs5{order:5}.flex.xs4{flex-basis:33.3333333333%;flex-grow:0;max-width:33.3333333333%}.flex.order-xs4{order:4}.flex.xs3{flex-basis:25%;flex-grow:0;max-width:25%}.flex.order-xs3{order:3}.flex.xs2{flex-basis:16.6666666667%;flex-grow:0;max-width:16.6666666667%}.flex.order-xs2{order:2}.flex.xs1{flex-basis:8.3333333333%;flex-grow:0;max-width:8.3333333333%}.flex.order-xs1{order:1}.v-application--is-ltr .flex.offset-xs12{margin-left:100%}.v-application--is-rtl .flex.offset-xs12{margin-right:100%}.v-application--is-ltr .flex.offset-xs11{margin-left:91.6666666667%}.v-application--is-rtl .flex.offset-xs11{margin-right:91.6666666667%}.v-application--is-ltr .flex.offset-xs10{margin-left:83.3333333333%}.v-application--is-rtl .flex.offset-xs10{margin-right:83.3333333333%}.v-application--is-ltr .flex.offset-xs9{margin-left:75%}.v-application--is-rtl .flex.offset-xs9{margin-right:75%}.v-application--is-ltr .flex.offset-xs8{margin-left:66.6666666667%}.v-application--is-rtl .flex.offset-xs8{margin-right:66.6666666667%}.v-application--is-ltr .flex.offset-xs7{margin-left:58.3333333333%}.v-application--is-rtl .flex.offset-xs7{margin-right:58.3333333333%}.v-application--is-ltr .flex.offset-xs6{margin-left:50%}.v-application--is-rtl .flex.offset-xs6{margin-right:50%}.v-application--is-ltr .flex.offset-xs5{margin-left:41.6666666667%}.v-application--is-rtl .flex.offset-xs5{margin-right:41.6666666667%}.v-application--is-ltr .flex.offset-xs4{margin-left:33.3333333333%}.v-application--is-rtl .flex.offset-xs4{margin-right:33.3333333333%}.v-application--is-ltr .flex.offset-xs3{margin-left:25%}.v-application--is-rtl .flex.offset-xs3{margin-right:25%}.v-application--is-ltr .flex.offset-xs2{margin-left:16.6666666667%}.v-application--is-rtl .flex.offset-xs2{margin-right:16.6666666667%}.v-application--is-ltr .flex.offset-xs1{margin-left:8.3333333333%}.v-application--is-rtl .flex.offset-xs1{margin-right:8.3333333333%}.v-application--is-ltr .flex.offset-xs0{margin-left:0}.v-application--is-rtl .flex.offset-xs0{margin-right:0}}@media (min-width:600px){.flex.sm12{flex-basis:100%;flex-grow:0;max-width:100%}.flex.order-sm12{order:12}.flex.sm11{flex-basis:91.6666666667%;flex-grow:0;max-width:91.6666666667%}.flex.order-sm11{order:11}.flex.sm10{flex-basis:83.3333333333%;flex-grow:0;max-width:83.3333333333%}.flex.order-sm10{order:10}.flex.sm9{flex-basis:75%;flex-grow:0;max-width:75%}.flex.order-sm9{order:9}.flex.sm8{flex-basis:66.6666666667%;flex-grow:0;max-width:66.6666666667%}.flex.order-sm8{order:8}.flex.sm7{flex-basis:58.3333333333%;flex-grow:0;max-width:58.3333333333%}.flex.order-sm7{order:7}.flex.sm6{flex-basis:50%;flex-grow:0;max-width:50%}.flex.order-sm6{order:6}.flex.sm5{flex-basis:41.6666666667%;flex-grow:0;max-width:41.6666666667%}.flex.order-sm5{order:5}.flex.sm4{flex-basis:33.3333333333%;flex-grow:0;max-width:33.3333333333%}.flex.order-sm4{order:4}.flex.sm3{flex-basis:25%;flex-grow:0;max-width:25%}.flex.order-sm3{order:3}.flex.sm2{flex-basis:16.6666666667%;flex-grow:0;max-width:16.6666666667%}.flex.order-sm2{order:2}.flex.sm1{flex-basis:8.3333333333%;flex-grow:0;max-width:8.3333333333%}.flex.order-sm1{order:1}.v-application--is-ltr .flex.offset-sm12{margin-left:100%}.v-application--is-rtl .flex.offset-sm12{margin-right:100%}.v-application--is-ltr .flex.offset-sm11{margin-left:91.6666666667%}.v-application--is-rtl .flex.offset-sm11{margin-right:91.6666666667%}.v-application--is-ltr .flex.offset-sm10{margin-left:83.3333333333%}.v-application--is-rtl .flex.offset-sm10{margin-right:83.3333333333%}.v-application--is-ltr .flex.offset-sm9{margin-left:75%}.v-application--is-rtl .flex.offset-sm9{margin-right:75%}.v-application--is-ltr .flex.offset-sm8{margin-left:66.6666666667%}.v-application--is-rtl .flex.offset-sm8{margin-right:66.6666666667%}.v-application--is-ltr .flex.offset-sm7{margin-left:58.3333333333%}.v-application--is-rtl .flex.offset-sm7{margin-right:58.3333333333%}.v-application--is-ltr .flex.offset-sm6{margin-left:50%}.v-application--is-rtl .flex.offset-sm6{margin-right:50%}.v-application--is-ltr .flex.offset-sm5{margin-left:41.6666666667%}.v-application--is-rtl .flex.offset-sm5{margin-right:41.6666666667%}.v-application--is-ltr .flex.offset-sm4{margin-left:33.3333333333%}.v-application--is-rtl .flex.offset-sm4{margin-right:33.3333333333%}.v-application--is-ltr .flex.offset-sm3{margin-left:25%}.v-application--is-rtl .flex.offset-sm3{margin-right:25%}.v-application--is-ltr .flex.offset-sm2{margin-left:16.6666666667%}.v-application--is-rtl .flex.offset-sm2{margin-right:16.6666666667%}.v-application--is-ltr .flex.offset-sm1{margin-left:8.3333333333%}.v-application--is-rtl .flex.offset-sm1{margin-right:8.3333333333%}.v-application--is-ltr .flex.offset-sm0{margin-left:0}.v-application--is-rtl .flex.offset-sm0{margin-right:0}}@media (min-width:960px){.flex.md12{flex-basis:100%;flex-grow:0;max-width:100%}.flex.order-md12{order:12}.flex.md11{flex-basis:91.6666666667%;flex-grow:0;max-width:91.6666666667%}.flex.order-md11{order:11}.flex.md10{flex-basis:83.3333333333%;flex-grow:0;max-width:83.3333333333%}.flex.order-md10{order:10}.flex.md9{flex-basis:75%;flex-grow:0;max-width:75%}.flex.order-md9{order:9}.flex.md8{flex-basis:66.6666666667%;flex-grow:0;max-width:66.6666666667%}.flex.order-md8{order:8}.flex.md7{flex-basis:58.3333333333%;flex-grow:0;max-width:58.3333333333%}.flex.order-md7{order:7}.flex.md6{flex-basis:50%;flex-grow:0;max-width:50%}.flex.order-md6{order:6}.flex.md5{flex-basis:41.6666666667%;flex-grow:0;max-width:41.6666666667%}.flex.order-md5{order:5}.flex.md4{flex-basis:33.3333333333%;flex-grow:0;max-width:33.3333333333%}.flex.order-md4{order:4}.flex.md3{flex-basis:25%;flex-grow:0;max-width:25%}.flex.order-md3{order:3}.flex.md2{flex-basis:16.6666666667%;flex-grow:0;max-width:16.6666666667%}.flex.order-md2{order:2}.flex.md1{flex-basis:8.3333333333%;flex-grow:0;max-width:8.3333333333%}.flex.order-md1{order:1}.v-application--is-ltr .flex.offset-md12{margin-left:100%}.v-application--is-rtl .flex.offset-md12{margin-right:100%}.v-application--is-ltr .flex.offset-md11{margin-left:91.6666666667%}.v-application--is-rtl .flex.offset-md11{margin-right:91.6666666667%}.v-application--is-ltr .flex.offset-md10{margin-left:83.3333333333%}.v-application--is-rtl .flex.offset-md10{margin-right:83.3333333333%}.v-application--is-ltr .flex.offset-md9{margin-left:75%}.v-application--is-rtl .flex.offset-md9{margin-right:75%}.v-application--is-ltr .flex.offset-md8{margin-left:66.6666666667%}.v-application--is-rtl .flex.offset-md8{margin-right:66.6666666667%}.v-application--is-ltr .flex.offset-md7{margin-left:58.3333333333%}.v-application--is-rtl .flex.offset-md7{margin-right:58.3333333333%}.v-application--is-ltr .flex.offset-md6{margin-left:50%}.v-application--is-rtl .flex.offset-md6{margin-right:50%}.v-application--is-ltr .flex.offset-md5{margin-left:41.6666666667%}.v-application--is-rtl .flex.offset-md5{margin-right:41.6666666667%}.v-application--is-ltr .flex.offset-md4{margin-left:33.3333333333%}.v-application--is-rtl .flex.offset-md4{margin-right:33.3333333333%}.v-application--is-ltr .flex.offset-md3{margin-left:25%}.v-application--is-rtl .flex.offset-md3{margin-right:25%}.v-application--is-ltr .flex.offset-md2{margin-left:16.6666666667%}.v-application--is-rtl .flex.offset-md2{margin-right:16.6666666667%}.v-application--is-ltr .flex.offset-md1{margin-left:8.3333333333%}.v-application--is-rtl .flex.offset-md1{margin-right:8.3333333333%}.v-application--is-ltr .flex.offset-md0{margin-left:0}.v-application--is-rtl .flex.offset-md0{margin-right:0}}@media (min-width:1264px){.flex.lg12{flex-basis:100%;flex-grow:0;max-width:100%}.flex.order-lg12{order:12}.flex.lg11{flex-basis:91.6666666667%;flex-grow:0;max-width:91.6666666667%}.flex.order-lg11{order:11}.flex.lg10{flex-basis:83.3333333333%;flex-grow:0;max-width:83.3333333333%}.flex.order-lg10{order:10}.flex.lg9{flex-basis:75%;flex-grow:0;max-width:75%}.flex.order-lg9{order:9}.flex.lg8{flex-basis:66.6666666667%;flex-grow:0;max-width:66.6666666667%}.flex.order-lg8{order:8}.flex.lg7{flex-basis:58.3333333333%;flex-grow:0;max-width:58.3333333333%}.flex.order-lg7{order:7}.flex.lg6{flex-basis:50%;flex-grow:0;max-width:50%}.flex.order-lg6{order:6}.flex.lg5{flex-basis:41.6666666667%;flex-grow:0;max-width:41.6666666667%}.flex.order-lg5{order:5}.flex.lg4{flex-basis:33.3333333333%;flex-grow:0;max-width:33.3333333333%}.flex.order-lg4{order:4}.flex.lg3{flex-basis:25%;flex-grow:0;max-width:25%}.flex.order-lg3{order:3}.flex.lg2{flex-basis:16.6666666667%;flex-grow:0;max-width:16.6666666667%}.flex.order-lg2{order:2}.flex.lg1{flex-basis:8.3333333333%;flex-grow:0;max-width:8.3333333333%}.flex.order-lg1{order:1}.v-application--is-ltr .flex.offset-lg12{margin-left:100%}.v-application--is-rtl .flex.offset-lg12{margin-right:100%}.v-application--is-ltr .flex.offset-lg11{margin-left:91.6666666667%}.v-application--is-rtl .flex.offset-lg11{margin-right:91.6666666667%}.v-application--is-ltr .flex.offset-lg10{margin-left:83.3333333333%}.v-application--is-rtl .flex.offset-lg10{margin-right:83.3333333333%}.v-application--is-ltr .flex.offset-lg9{margin-left:75%}.v-application--is-rtl .flex.offset-lg9{margin-right:75%}.v-application--is-ltr .flex.offset-lg8{margin-left:66.6666666667%}.v-application--is-rtl .flex.offset-lg8{margin-right:66.6666666667%}.v-application--is-ltr .flex.offset-lg7{margin-left:58.3333333333%}.v-application--is-rtl .flex.offset-lg7{margin-right:58.3333333333%}.v-application--is-ltr .flex.offset-lg6{margin-left:50%}.v-application--is-rtl .flex.offset-lg6{margin-right:50%}.v-application--is-ltr .flex.offset-lg5{margin-left:41.6666666667%}.v-application--is-rtl .flex.offset-lg5{margin-right:41.6666666667%}.v-application--is-ltr .flex.offset-lg4{margin-left:33.3333333333%}.v-application--is-rtl .flex.offset-lg4{margin-right:33.3333333333%}.v-application--is-ltr .flex.offset-lg3{margin-left:25%}.v-application--is-rtl .flex.offset-lg3{margin-right:25%}.v-application--is-ltr .flex.offset-lg2{margin-left:16.6666666667%}.v-application--is-rtl .flex.offset-lg2{margin-right:16.6666666667%}.v-application--is-ltr .flex.offset-lg1{margin-left:8.3333333333%}.v-application--is-rtl .flex.offset-lg1{margin-right:8.3333333333%}.v-application--is-ltr .flex.offset-lg0{margin-left:0}.v-application--is-rtl .flex.offset-lg0{margin-right:0}}@media (min-width:1904px){.flex.xl12{flex-basis:100%;flex-grow:0;max-width:100%}.flex.order-xl12{order:12}.flex.xl11{flex-basis:91.6666666667%;flex-grow:0;max-width:91.6666666667%}.flex.order-xl11{order:11}.flex.xl10{flex-basis:83.3333333333%;flex-grow:0;max-width:83.3333333333%}.flex.order-xl10{order:10}.flex.xl9{flex-basis:75%;flex-grow:0;max-width:75%}.flex.order-xl9{order:9}.flex.xl8{flex-basis:66.6666666667%;flex-grow:0;max-width:66.6666666667%}.flex.order-xl8{order:8}.flex.xl7{flex-basis:58.3333333333%;flex-grow:0;max-width:58.3333333333%}.flex.order-xl7{order:7}.flex.xl6{flex-basis:50%;flex-grow:0;max-width:50%}.flex.order-xl6{order:6}.flex.xl5{flex-basis:41.6666666667%;flex-grow:0;max-width:41.6666666667%}.flex.order-xl5{order:5}.flex.xl4{flex-basis:33.3333333333%;flex-grow:0;max-width:33.3333333333%}.flex.order-xl4{order:4}.flex.xl3{flex-basis:25%;flex-grow:0;max-width:25%}.flex.order-xl3{order:3}.flex.xl2{flex-basis:16.6666666667%;flex-grow:0;max-width:16.6666666667%}.flex.order-xl2{order:2}.flex.xl1{flex-basis:8.3333333333%;flex-grow:0;max-width:8.3333333333%}.flex.order-xl1{order:1}.v-application--is-ltr .flex.offset-xl12{margin-left:100%}.v-application--is-rtl .flex.offset-xl12{margin-right:100%}.v-application--is-ltr .flex.offset-xl11{margin-left:91.6666666667%}.v-application--is-rtl .flex.offset-xl11{margin-right:91.6666666667%}.v-application--is-ltr .flex.offset-xl10{margin-left:83.3333333333%}.v-application--is-rtl .flex.offset-xl10{margin-right:83.3333333333%}.v-application--is-ltr .flex.offset-xl9{margin-left:75%}.v-application--is-rtl .flex.offset-xl9{margin-right:75%}.v-application--is-ltr .flex.offset-xl8{margin-left:66.6666666667%}.v-application--is-rtl .flex.offset-xl8{margin-right:66.6666666667%}.v-application--is-ltr .flex.offset-xl7{margin-left:58.3333333333%}.v-application--is-rtl .flex.offset-xl7{margin-right:58.3333333333%}.v-application--is-ltr .flex.offset-xl6{margin-left:50%}.v-application--is-rtl .flex.offset-xl6{margin-right:50%}.v-application--is-ltr .flex.offset-xl5{margin-left:41.6666666667%}.v-application--is-rtl .flex.offset-xl5{margin-right:41.6666666667%}.v-application--is-ltr .flex.offset-xl4{margin-left:33.3333333333%}.v-application--is-rtl .flex.offset-xl4{margin-right:33.3333333333%}.v-application--is-ltr .flex.offset-xl3{margin-left:25%}.v-application--is-rtl .flex.offset-xl3{margin-right:25%}.v-application--is-ltr .flex.offset-xl2{margin-left:16.6666666667%}.v-application--is-rtl .flex.offset-xl2{margin-right:16.6666666667%}.v-application--is-ltr .flex.offset-xl1{margin-left:8.3333333333%}.v-application--is-rtl .flex.offset-xl1{margin-right:8.3333333333%}.v-application--is-ltr .flex.offset-xl0{margin-left:0}.v-application--is-rtl .flex.offset-xl0{margin-right:0}}.child-flex>*,.flex{flex:1 1 auto;max-width:100%}.child-flex>.grow-shrink-0,.flex.grow-shrink-0{flex-grow:0;flex-shrink:0}.grow,.spacer{flex-grow:1!important}.grow{flex-shrink:0!important}.shrink{flex-grow:0!important;flex-shrink:1!important}.fill-height{height:100%}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 163:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(164);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("0cd63bd9", content, true)

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".v-parallax{position:relative;overflow:hidden;z-index:0}.v-parallax__image-container{position:absolute;top:0;left:0;right:0;bottom:0;z-index:1;contain:strict}.v-parallax__image{position:absolute;bottom:0;left:50%;min-width:100%;min-height:100%;display:none;transform:translate(-50%);will-change:transform;transition:opacity .3s cubic-bezier(.25,.8,.5,1);z-index:1}.v-parallax__content{color:#fff;height:100%;z-index:2;position:relative;display:flex;flex-direction:column;justify-content:center;padding:0 1rem}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VGrid/_grid.sass
var _grid = __webpack_require__(157);

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VGrid/VGrid.sass
var VGrid = __webpack_require__(51);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(0);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VGrid/grid.js
// Types

function grid_VGrid(name) {
  /* @vue/component */
  return external_vue_default.a.extend({
    name: `v-${name}`,
    functional: true,
    props: {
      id: String,
      tag: {
        type: String,
        default: 'div'
      }
    },

    render(h, {
      props,
      data,
      children
    }) {
      data.staticClass = `${name} ${data.staticClass || ''}`.trim();
      const {
        attrs
      } = data;

      if (attrs) {
        // reset attrs to extract utility clases like pa-3
        data.attrs = {};
        const classes = Object.keys(attrs).filter(key => {
          // TODO: Remove once resolved
          // https://github.com/vuejs/vue/issues/7841
          if (key === 'slot') return false;
          const value = attrs[key]; // add back data attributes like data-test="foo" but do not
          // add them as classes

          if (key.startsWith('data-')) {
            data.attrs[key] = value;
            return false;
          }

          return value || typeof value === 'string';
        });
        if (classes.length) data.staticClass += ` ${classes.join(' ')}`;
      }

      if (props.id) {
        data.domProps = data.domProps || {};
        data.domProps.id = props.id;
      }

      return h(props.tag, data, children);
    }

  });
}
// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mergeData.js
var mergeData = __webpack_require__(12);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js




/* @vue/component */

/* harmony default export */ var VContainer = __webpack_exports__["a"] = (grid_VGrid('container').extend({
  name: 'v-container',
  functional: true,
  props: {
    id: String,
    tag: {
      type: String,
      default: 'div'
    },
    fluid: {
      type: Boolean,
      default: false
    }
  },

  render(h, {
    props,
    data,
    children
  }) {
    let classes;
    const {
      attrs
    } = data;

    if (attrs) {
      // reset attrs to extract utility clases like pa-3
      data.attrs = {};
      classes = Object.keys(attrs).filter(key => {
        // TODO: Remove once resolved
        // https://github.com/vuejs/vue/issues/7841
        if (key === 'slot') return false;
        const value = attrs[key]; // add back data attributes like data-test="foo" but do not
        // add them as classes

        if (key.startsWith('data-')) {
          data.attrs[key] = value;
          return false;
        }

        return value || typeof value === 'string';
      });
    }

    if (props.id) {
      data.domProps = data.domProps || {};
      data.domProps.id = props.id;
    }

    return h(props.tag, Object(mergeData["a" /* default */])(data, {
      staticClass: 'container',
      class: Array({
        'container--fluid': props.fluid
      }).concat(classes || [])
    }), children);
  }

}));

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VParallax/VParallax.sass
var VParallax = __webpack_require__(163);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(0);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/mixins/translatable/index.js

/* harmony default export */ var translatable = (external_vue_default.a.extend({
  name: 'translatable',
  props: {
    height: Number
  },
  data: () => ({
    elOffsetTop: 0,
    parallax: 0,
    parallaxDist: 0,
    percentScrolled: 0,
    scrollTop: 0,
    windowHeight: 0,
    windowBottom: 0
  }),
  computed: {
    imgHeight() {
      return this.objHeight();
    }

  },

  beforeDestroy() {
    window.removeEventListener('scroll', this.translate, false);
    window.removeEventListener('resize', this.translate, false);
  },

  methods: {
    calcDimensions() {
      const offset = this.$el.getBoundingClientRect();
      this.scrollTop = window.pageYOffset;
      this.parallaxDist = this.imgHeight - this.height;
      this.elOffsetTop = offset.top + this.scrollTop;
      this.windowHeight = window.innerHeight;
      this.windowBottom = this.scrollTop + this.windowHeight;
    },

    listeners() {
      window.addEventListener('scroll', this.translate, false);
      window.addEventListener('resize', this.translate, false);
    },

    /** @abstract **/
    objHeight() {
      throw new Error('Not implemented !');
    },

    translate() {
      this.calcDimensions();
      this.percentScrolled = (this.windowBottom - this.elOffsetTop) / (parseInt(this.height) + this.windowHeight);
      this.parallax = Math.round(this.parallaxDist * this.percentScrolled);
    }

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VParallax/VParallax.js
// Style
 // Mixins



const baseMixins = Object(mixins["a" /* default */])(translatable);
/* @vue/component */

/* harmony default export */ var VParallax_VParallax = __webpack_exports__["a"] = (baseMixins.extend().extend({
  name: 'v-parallax',
  props: {
    alt: {
      type: String,
      default: ''
    },
    height: {
      type: [String, Number],
      default: 500
    },
    src: String,
    srcset: String
  },
  data: () => ({
    isBooted: false
  }),
  computed: {
    styles() {
      return {
        display: 'block',
        opacity: this.isBooted ? 1 : 0,
        transform: `translate(-50%, ${this.parallax}px)`
      };
    }

  },

  mounted() {
    this.init();
  },

  methods: {
    init() {
      const img = this.$refs.img;
      if (!img) return;

      if (img.complete) {
        this.translate();
        this.listeners();
      } else {
        img.addEventListener('load', () => {
          this.translate();
          this.listeners();
        }, false);
      }

      this.isBooted = true;
    },

    objHeight() {
      return this.$refs.img.naturalHeight;
    }

  },

  render(h) {
    const imgData = {
      staticClass: 'v-parallax__image',
      style: this.styles,
      attrs: {
        src: this.src,
        srcset: this.srcset,
        alt: this.alt
      },
      ref: 'img'
    };
    const container = h('div', {
      staticClass: 'v-parallax__image-container'
    }, [h('img', imgData)]);
    const content = h('div', {
      staticClass: 'v-parallax__content'
    }, this.$slots.default);
    return h('div', {
      staticClass: 'v-parallax',
      style: {
        height: `${this.height}px`
      },
      on: this.$listeners
    }, [container, content]);
  }

}));

/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/store1.9d123e9.jpg";

/***/ }),

/***/ 198:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/logo-emtrade-white.3e7cbb6.png";

/***/ }),

/***/ 199:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member1.51c2dae.png";

/***/ }),

/***/ 200:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member2.9fa3b68.png";

/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member3.d3ce4ae.png";

/***/ }),

/***/ 202:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member4.9c46357.png";

/***/ }),

/***/ 203:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member5.f3233bd.png";

/***/ }),

/***/ 204:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member6.03c8e93.png";

/***/ }),

/***/ 205:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/member7.8c20be9.png";

/***/ }),

/***/ 206:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(243);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("e9278712", content, true, context)
};

/***/ }),

/***/ 238:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/store3.7b043a4.jpg";

/***/ }),

/***/ 239:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/store4.338beea.jpg";

/***/ }),

/***/ 240:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/store5.1f2afcd.png";

/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/store7.0d0a1b7.jpg";

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_store_vue_vue_type_style_index_0_id_630830b5_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(206);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_store_vue_vue_type_style_index_0_id_630830b5_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_store_vue_vue_type_style_index_0_id_630830b5_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_store_vue_vue_type_style_index_0_id_630830b5_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_store_vue_vue_type_style_index_0_id_630830b5_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".name-text[data-v-630830b5]{font-weight:500;font-size:26px}.name-text[data-v-630830b5],.sub-text[data-v-630830b5]{font-style:normal;line-height:30px;text-align:center}.sub-text[data-v-630830b5]{font-weight:400;font-size:18px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/store.vue?vue&type=template&id=630830b5&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '800' : '600',"src":__webpack_require__(186)}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":_vm.screenWidth < 400 ? '200' : '500',"lazy-src":__webpack_require__(198),"src":__webpack_require__(198)}})],1),_vm._v(" "),_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title-2 mt-10 teal--text text--lighten-2"},[_vm._v("\n              Now everyone can trade!\n            ")]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_vm._v("\n              EMTrade adalah aplikasi yang memberimu solusi untuk lebih untung\n              dan meminimalisir risiko dalam jual beli saham.\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v(" "),_c('b',{staticClass:"white--text"},[_vm._v("EMtrade memberi edukasi saham online secara intensif")]),_vm._v("\n              dengan bonus GRATIS referensi saham baik untuk beli jual saham\n              yang berkinerja positif sejak 2016.\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v(" "),_c('b',{staticClass:"white--text"},[_vm._v("Konsultasi • Edukasi • Real Trading")])]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(199),"src":__webpack_require__(199)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Ubai")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Member Emtrade")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Sejak saya menjadi user EMtrade, saya merasa lebih terarah, serta\n            bisa menghasilkan profit berkali-kali lipat dari sebelumnya,\n            EMtrade seperti kompas, saya bisa tahu arah. Keuntungan sy\n            meningkat +/-45% rata2 di atas 50%. Salam profit!\"\n          ")])])])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '800' : '500',"src":__webpack_require__(186)}},[_c('v-container',[_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title mt-10"},[_vm._v("Seminar Online 2021")]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_vm._v("\n              Melalui seminar online ini Anda akan dapatkan info tentang\n              bagaimana berinvestasi saham ketika pasar saham sedang diskon\n              besar, beli saham apa, di harga berapa, dan berapa banyak boleh\n              masuk.\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n              by Ellen May • bonus "),_c('b',{staticClass:"white--text"},[_vm._v("GRATIS")]),_vm._v(" Ebook\n              ter-Update • gratis sertifikat keikutsertaan\n            ")]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(200),"src":__webpack_require__(200)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Robert Aditya")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Member Emtrade")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Saya Robert Aditya, melalui seminar Diskonvaganza setiap Sabtu\n            saya termotivasi untuk menatap peluang ditengah market yang tidak\n            pasti. Update market dan arahan Miss Ellen dan EM Trade team dalam\n            setiap trading terutama swing dalam jangka pendek ini telah\n            terbukti menghasilkan profit untuk saya sebesar 22% untuk saham\n            ASII. Ini adalah achievement luar biasa sebagai trader junior.\n            Juga untuk berapa saham seperti ELSA, INAF, dan KAEF telah memberi\n            profit 3-6%. Terima kasih atas support dan motivasi dari Miss\n            Ellen dalam mengedukasi masyarakat dan juga EM Trade team yang\n            mengarahkan kita dalam melakukan trading. Semoga badai ini segera\n            berlalu, pasar kembali bergairah dan perekonomian kembali pulih.\"\n          ")])])])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '1000' : '700',"src":__webpack_require__(238)}},[_c('v-container',[_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title-2 mt-10 white--text"},[_vm._v("\n              Baru mulai investasi saham? Ingin buka rekening saham pertama?\n              Ingin tau dasar mekanisme pasar saham?\n            ")]),_vm._v(" "),_c('div',{staticClass:"paralax-title-2 mt-10 white--text"},[_vm._v("Ikuti Program")]),_vm._v(" "),_c('div',{staticClass:"paralax-title mt-10"},[_c('b',{staticClass:"orange--text text--accent-1"},[_vm._v("INVESTART")])]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_c('b',{staticClass:"orange--text text--accent-1"},[_vm._v("INVESTART")]),_vm._v(" adalah\n              kelas edukasi saham untuk pemula,"),_c('br'),_vm._v("yang baru mau buka\n              rekening saham pertamanya.\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n\n              Anda akan mendapatkan edukasi saham dan"),_c('br'),_vm._v("dibantu untuk\n              membuka rekening saham pertama Anda!\n            ")]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(201),"src":__webpack_require__(201)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Rusli")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Alumni Super Trader Workshop")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Sebelum ikut workshop Super Performance Trader, awalnya tidak\n            tahu apa-apa sampai dibuat paham tentang saham oleh Ellen May.\n            Setelah Super Trader Workshop, saya bisa profit 21 % dalam 2 bulan\n            buat liburan. Terima kasih Ms Ellen dan team. Saya pernah rugi\n            juga, karena tidak ada bisnis yang tidak rugi. Yang penting\n            kerugian dibatasi. Pokoke untungnyaa puas puas puas dan puasssss.\n            \"\n          ")])])])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '800' : '500',"src":__webpack_require__(239)}},[_c('v-container',[_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title mt-10"},[_vm._v("Workshop Super Trader")]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_vm._v("\n              Super Trader™ EXPRESS workshop merupakan pelatihan eksklusif 2\n              hari yang diajarkan langsung oleh Ellen May dan berisi\n              strategi-strategi practical untuk dapatkan keuntungan hingga 3\n              digit di pasar saham.\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n              • Cara mencari saham super • meminimalisir risiko •\n              mengendalikan rasa takut dan serakah • studi kasus nyata •\n              gratis ikut lagi • dibimbing setelah pelatihan dengan EMTrade\n            ")]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(202),"src":__webpack_require__(202)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Andika")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Alumni Super Trader Workshop")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Saat saya ikut Super Trader Workshop sempat pesimis, apa bisa\n            profit via trading. Tapi sekarang saya sudah membuktikan sendiri\n            bisa profit puluhan, bahkan ratusan persen, 137 % hanya dalam 3\n            minggu. Terima kasih sekali bu Ellen, untuk ilmu yang ibu share ke\n            kami. Saya jadi bisa mendapat mentor yg keren but simple.\"\n          ")])])])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '1000' : '700',"src":__webpack_require__(240)}},[_c('v-container',[_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title mt-10"},[_vm._v("\n              Online Class Trading dan Investasi Saham\n            ")]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_c('span',{staticClass:"teal--text text--lighten-2"},[_vm._v("Mau belajar saham di mana saja dan kapan saja?"),_c('br'),_vm._v("\n                Bisa diulang sesuka-suka Anda!")]),_vm._v(" "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n              Merupakan layanan edukasi online, berupa video, oleh Ellen May,\n              yang bisa Anda tonton berulang-ulang kapan pun di manapun,\n              dengan praktis dan fun, sangat cocok untuk pemula\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n              • dasar trading dan investasi saham • dasar analisis teknikal •\n              trading for living\n            ")]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(203),"src":__webpack_require__(203)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Krisna Johan")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Alumni Super Trader Workshop")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Setelah ikut Workshop Super Performance Trader dari Ellen May\n            Institute, saya bisa profit 400 % dalam 2 tahun sehingga saya bisa\n            liburan melihat salju. Salam profit, terima kasih Ms Ellen May\n            bersama team Ellen May Institute yang sangat oke punya.\"\n          ")])])])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '1000' : '600',"src":__webpack_require__(186)}},[_c('v-container',[_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title mt-10"},[_vm._v("Seminar Online Super Trader")]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_c('span',{staticClass:"teal--text text--lighten-2"},[_vm._v("3 langkah mulai trading saham"),_c('br'),_vm._v("\n                agar keuntungan optimal & risiko terkendal")]),_vm._v(" "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n              Dengan belajar bersama Seminar Online Super Trader ini Anda bisa\n              tahu tentang strategi beli jual saham, memilih saham yang profit\n              optimal, dan juga cara meminimalisir risiko dalam beli jual\n              saham bersama Ellen May @pakarsaham secara langsung\n              "),_c('br'),_vm._v(" "),_c('br'),_vm._v("\n              • by Ellen May • bonus GRATIS Ebook Super Trader • gratis\n              sertifikat keikutsertaan\n            ")]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(204),"src":__webpack_require__(204)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Tugar - Yogyakarta")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Alumni Super Trader Workshop")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Setelah ikut Super Trader Workshop saya bisa profit 100 % dalam 3\n            bulan dari saham DOID. Terima kasih Ms Ellen dan team.\"\n          ")])])])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-parallax',{staticClass:"mt-3",attrs:{"height":_vm.screenWidth < 400 ? '900' : '500',"src":__webpack_require__(241)}},[_c('v-container',[_c('div',{staticClass:"paralax-text"},[_c('div',{staticClass:"paralax-title mt-10"},[_vm._v("Buku Karya Ellen May")]),_vm._v(" "),_c('div',{staticClass:"paralax-description"},[_c('span',{staticClass:"teal--text text--lighten-2"},[_vm._v("Pelajari investasi dan trading saham dari 3 buku National\n                Best Selling karya Ellen May")])]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('ol',{staticClass:"text-left"},[_c('li',{staticClass:"white--text"},[_vm._v("\n                  Smart Traders Not Gamblers adalah buku tentang swing trading\n                  & psikologi trading, cocok untuk trader saham dan forex.\n                ")]),_vm._v(" "),_c('li',{staticClass:"white--text"},[_vm._v("\n                  Smart Trader Rich Investor adalah buku pengenalan investasi\n                  saham, cocok untuk pemula yang ingin belajar mengenal\n                  investasi saham dan reksadana.\n                ")]),_vm._v(" "),_c('li',{staticClass:"white--text"},[_vm._v("\n                  Nabung Saham Sekarang adalah buku panduan untuk nabung\n                  saham, lengkap dengan saham rekomendasi saham dan cara beli\n                  jual.\n                ")])])]),_vm._v(" "),_c('div',{staticClass:"mt-10"},[_c('v-btn',{attrs:{"outlined":"","color":"white"}},[_vm._v(" Daftar ")])],1)])])],1)],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-630830b5>","</div>",[_c('v-lazy',{attrs:{"options":{
        threshold: 0.5,
      },"min-height":"200","transition":"fade-transition"}},[_c('v-container',[_c('div',[_c('v-img',{staticClass:"mx-auto",attrs:{"width":"175","height":"181","lazy-src":__webpack_require__(205),"src":__webpack_require__(205)}})],1),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('div',{staticClass:"name-text"},[_vm._v("Evi Krismawati")]),_vm._v(" "),_c('div',{staticClass:"sub-text grey--text"},[_vm._v("Member EMtrade")])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"my-3"},[_c('p',{staticClass:"text-center"},[_vm._v("\n            \"Haloo, sy baru gabung ikut member EMTrade.. Kmrn mengikuti saran\n            swing Trading Dr EMTrade.. Minggu lalu baru 2x jualan saham Elsa,\n            Beli 205 jual 214, untung 4% WSKT, beli 625 jual 700 untung 12%\n            Dari program morning briefing, seminar online, buku2 Miss Ellen\n            banyak membantu sy yg newbie utk transaksi saham.. Tetap semangat\n            Miss Ellen dan Tim, salam profit!!\"\n          ")])])])],1)],1)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/store.vue?vue&type=template&id=630830b5&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/store.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var storevue_type_script_lang_js_ = ({
  data: () => ({
    screenWidth: 0,
    contents: [{
      image: 'content1.png',
      description: 'Article',
      to: ''
    }, {
      image: 'content2.png',
      description: 'Videos',
      to: ''
    }, {
      image: 'content3.png',
      description: 'Blogs by Ellen May',
      to: ''
    }, {
      image: 'content4.png',
      description: 'Blogs by Ellen May',
      to: ''
    }]
  }),
  watch: {
    $vssWidth() {
      this.screenWidth = this.$vssWidth;
    }

  },

  mounted() {
    this.screenWidth = this.$vssWidth;
  }

});
// CONCATENATED MODULE: ./pages/store.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_storevue_type_script_lang_js_ = (storevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(15);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(54);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js + 1 modules
var VContainer = __webpack_require__(167);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(83);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VLazy/VLazy.js
var VLazy = __webpack_require__(154);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VParallax/VParallax.js + 1 modules
var VParallax = __webpack_require__(183);

// CONCATENATED MODULE: ./pages/store.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(242)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_storevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "630830b5",
  "5ca6ff92"
  
)

/* harmony default export */ var store = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */






installComponents_default()(component, {VBtn: VBtn["a" /* default */],VContainer: VContainer["a" /* default */],VImg: VImg["a" /* default */],VLazy: VLazy["a" /* default */],VParallax: VParallax["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=store.js.map