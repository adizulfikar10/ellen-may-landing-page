import colors from "vuetify/es5/util/colors";

export default {
  // mode: 'spa',
  // devtools: true,
  // target: 'static',
  // ssr: false,
  mode: "universal",
  server: {
    host: "0.0.0.0" // default: localhost
  },

  loading: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - Emtrade",
    title: "Emtrade",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Montserrat&display=swap"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/main.scss", "hooper/dist/hooper.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/screenSize.js", ssr: false },
    { src: "~/plugins/aos", ssr: false },
    { src: "~/plugins/socialSharing.js", ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
    // compression
    "nuxt-compress",
    // analytic
    "@nuxtjs/google-analytics"
  ],

  googleAnalytics: {
    id: "UA-115649554-2"
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    // '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    "@nuxt/content",
    // social sharing
    "vue-social-sharing/nuxt",
    // nuxt proxy
    "@nuxtjs/proxy",
    // sitemap
    "@nuxtjs/sitemap",
    // Toast
    "@nuxtjs/toast",
    // compression
    [
      "nuxt-compress",
      {
        gzip: {
          cache: true
        },
        brotli: {
          threshold: 10240
        }
      }
    ]
  ],

  sitemap: {
    hostname: "https://emtrade.id",
    gzip: true,
    // exclude: ['/secret', '/admin/**'],
    routes: ["dynamic-route-one", "dynamic-route-two"]
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // baseURL: process.env.VUE_APP_FREEMIUM_API_URL,
    proxy: true
  },

  proxy: {
    "/article/": {
      target: process.env.VUE_APP_FREEMIUM_API_URL,
      pathRewrite: { "^/article/": "" }
    },
    "/obvest/": {
      target: process.env.VUE_APP_OBVEST,
      pathRewrite: { "^/obvest/": "" }
    }
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  // pwa: {
  //   manifest: {
  //     lang: 'en',
  //   },
  // },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};
